README file for the OG Public Access Drupal module.


Description
***********

The OG Public Access module allows Organic Groups (OG) administrators to control
public access to their groups' content. This module does not require group
administrators to be granted the site-wide "administer nodes" permission.

The use case targeted by this module is for a site to have teams of writers
submitting content privately in their groups, with editors controlling what is
allowed to go public in specific groups.

The workflow is very similar to Modr8's [http://drupal.org/project/modr8].

If all the group administrators have the "administer nodes" permission, you do
not need the OG Public Access module. Modr8 would do fine.

OG Public Access works by manipulating data in the node_access table. I'd be
glad to get feedback from the community about the way this module plays with
node_access. Porting this to Drupal 5.x will probably require some
re-thinking... Contributions are welcome!


Installation and configuration
******************************

1. Copy the 'og_public_access' directory and its content into your Drupal
   modules directory.

2. Grant the "moderate public access to content in groups administered by self"
   permission to appropriate roles. Users will need to have an administrator
   status in their groups to be able to moderate public access to content in
   those groups.

3. If your group administrators do not have the "administer nodes" permission,
   then you'll have to set these defaults to node types used in groups
   (admin/settings/content-types):

   * Published: Yes
   * In moderation: No

   You need this, unless you want people with "administer nodes" permission to
   also have to go through every node to publish them.

4. In the Organic Groups settings (admin/settings/og), select "Visible only
   within the targeted groups" in the "Visibility of posts" section.

5. [Optional] Configure the behavior of og_public_access to your liking in its
   settings page (admin/settings/og_public_access).


Usage
*****

Group subscribers can create content as usual.

Group administrators should go to "moderate public access to group content"
(admin/og_public_access/moderate) to grant public access to new posts in any of
their groups. For each group into which the content has been posted and for
which the user is an administrator, the user is given the choice to approve,
reject or do nothing.

Approving a post makes it accessible to non-subscribers of the group. Rejecting
removes it from the group (but does not delete it). Doing nothing leaves the
post in the queue for later approval, and also leaves it accessible to group
subscribers.


Caveats
*******

When a post is rejected from each of the groups it had originally been posted
to, it remains in the system but is no longer associated to any group and is
accessible only to users with "administer nodes" permission.


Credits
*******

David Lesieur, http://davidlesieur.com.

This module was developed for a client of Koumbit.org (http://koumbit.org).
